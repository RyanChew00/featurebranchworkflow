﻿using System;

namespace FeatureBranchWorkflow_ryan.yujie
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int sum = 0;

            Console.Write("Please enter a number : ");
            int input = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= input; i++)
            {
                Console.WriteLine(i);
                sum += i;
            }

            Console.WriteLine("The sum is: " + sum);

        }
    }
}
